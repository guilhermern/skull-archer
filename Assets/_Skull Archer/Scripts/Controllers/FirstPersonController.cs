﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPersonController : MonoBehaviour
{
    public float horizontalCameraSpeed;
    public float verticalCameraSpeed;
    public float movementSpeed;
    public float jumpForce;

    private float horizontalInput;
    private float verticalInput;

    private float pitch = 0.0f;
    private float yaw = 0.0f;

    private Rigidbody rigidb;
    private Camera camera;
    private Vector3 finalVel;

    private bool jumping = false;

    private void Start ()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        rigidb = GetComponent<Rigidbody>();
        camera = Camera.main;
    }
	
	
	private void Update ()
    {
        if (GameManager.Instance.state != GameState.Playing)
            return;

        //get mouse input
        yaw += horizontalCameraSpeed * Input.GetAxis("Mouse X");
        pitch += verticalCameraSpeed * Input.GetAxis("Mouse Y");
        pitch = Mathf.Clamp(pitch, -60f, 60f);
        
        //makes the player yaw and camera pitch
        transform.eulerAngles = new Vector3(0.0f, yaw, 0.0f);
        camera.transform.eulerAngles = new Vector3(-pitch, camera.transform.eulerAngles.y, 0.0f);

        //update player movement
        horizontalInput = Input.GetAxis("Horizontal");
        verticalInput = Input.GetAxis("Vertical");

        finalVel = (transform.forward * movementSpeed * verticalInput) + (transform.right * movementSpeed * horizontalInput);


        rigidb.velocity = new Vector3(finalVel.x, rigidb.velocity.y, finalVel.z);
        //rigidb.velocity = finalVel;

        //jump
        if (Input.GetKeyUp(KeyCode.Space) && !jumping)
        {
            rigidb.AddForce(new Vector3(0f, jumpForce, 0f));
            jumping = true;
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.tag == "Ground")
            jumping = false;
    }
}
