﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcherController : Enemy
{
    public GameObject arrowPrefab;
    public GameObject arrowSpot;

    //public int fireForce;
    private List<GameObject> arrows = new List<GameObject>();
    private int poolAmount = 4;

    protected override void OnEnable()
    {
        base.OnEnable();
        stateMachine.ChangeState(new IdleState());

        //pool arrows
        for (int i = 0; i < poolAmount; i++)
        {
            GameObject obj = Instantiate(arrowPrefab);
            obj.SetActive(false);
            obj.GetComponent<BulletController>().owner = this;
            obj.transform.localScale = new Vector3(.8f, .8f, .8f);
            arrows.Add(obj);
        }
    }

    protected override void Update()
    {
        base.Update();
    }

    public void ShotArrow()
    {
        for (int i = 0; i < arrows.Count; i++)
        {
            if (!arrows[i].activeInHierarchy)
            {
                arrows[i].transform.position = arrowSpot.transform.position;
                arrows[i].transform.eulerAngles = arrowSpot.transform.eulerAngles;
                arrows[i].transform.LookAt(GameManager.Instance.player.transform);
                arrows[i].SetActive(true);
                break;
            }
        }                
    }
}
