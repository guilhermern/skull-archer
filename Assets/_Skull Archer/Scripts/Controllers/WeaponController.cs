﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponController : MonoBehaviour
{
    public GameObject bullet;
    public GameObject staticBullet;

    public float fireTime;
    public int initialAmmo { get; set; }

    public Text uiArrowsValue;
    private int currentAmmo;

    private List<GameObject> bullets = new List<GameObject>();
    private int pooledAmount = 8;
    private float currentFireTime = 0f;

    private void Start()
    {
        //pooling arrows
        for (int i = 0; i < pooledAmount; i++)
        {
            GameObject obj = Instantiate(bullet);
            obj.SetActive(false);
            bullets.Add(obj);
        }

        
    }

    private void Update()
    {
        if (GameManager.Instance.state != GameState.Playing)
            return;

        //fire the bullet and enables the static bullet when necessary
        if (Input.GetKeyDown(KeyCode.Mouse0) && currentFireTime <= 0f && currentAmmo > 0)
        {
            Fire();
            currentFireTime = fireTime;
            currentAmmo--;
            uiArrowsValue.text = currentAmmo.ToString();
        }

        if (currentFireTime > 0f)
            currentFireTime -= Time.deltaTime;

        if (currentFireTime <= 0f && !staticBullet.activeInHierarchy && currentAmmo > 0)
        {
            staticBullet.SetActive(true);
            currentFireTime = 0f;
        }

    }


    public void SetInitialAmmo(int value)
    {
        initialAmmo = value;
        uiArrowsValue.text = initialAmmo.ToString();
        currentAmmo = initialAmmo;
    }

    public void AddAmmo(int quantity)
    {
        currentAmmo += quantity;
        uiArrowsValue.text = currentAmmo.ToString();
    }

    private void Fire()
    {
        for (int i = 0; i < bullets.Count; i++)
        {
            if (!bullets[i].activeInHierarchy)
            {
                staticBullet.SetActive(false);
                bullets[i].transform.position = staticBullet.transform.position;
                bullets[i].transform.eulerAngles = staticBullet.transform.eulerAngles;
                bullets[i].SetActive(true);
                break;
            }
        }
    }

}
