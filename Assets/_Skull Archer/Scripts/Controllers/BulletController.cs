﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    public float fireForce;
    private Rigidbody rb;
    private TrailRenderer trail;
    private AudioSource audioSource;


    public Enemy owner { get; set; }

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        trail = GetComponentInChildren<TrailRenderer>();
        trail.enabled = false;
    }

    private void OnEnable()
    {
        //get the forward direction and add force
        rb.AddForce( transform.forward * fireForce);
        trail.enabled = true;
    }

    private void OnDisable()
    {
        trail.enabled = false;
    }

    private void OnCollisionEnter(Collision other)
    {
        switch (other.gameObject.tag)
        {
            case "Ground":
                rb.velocity = Vector3.zero;
                gameObject.SetActive(false);
                break;

            case "Enemy":
                if (owner == null)
                {
                    rb.velocity = Vector3.zero;
                    int attack = GameManager.Instance.player.GetComponent<PlayerController>().attackValue;
                    other.gameObject.GetComponent<Enemy>().TakeDamage(attack);
                }
                gameObject.SetActive(false);
                break;

            case "Player":
                other.gameObject.GetComponent<PlayerController>().TakeDamage(owner.attackValue);
                gameObject.SetActive(false);
                break;
        }
    }
}
