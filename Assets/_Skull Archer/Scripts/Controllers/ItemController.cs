﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ItemType { Medkit, Ammo }
    
public class ItemController : MonoBehaviour
{
    public ItemType type;
    public float lifeTime;
    protected Rigidbody rigidb;

    private float rotatationSpeed = 3f;
    private float rotationDegree = 20f;

    private Transform meshTransform;

    private void Awake()
    {
        rigidb = GetComponentInChildren<Rigidbody>();
    }

    private void Start()
    {
    }

    private void Update()
    {
        meshTransform.Rotate(0, (Time.deltaTime * rotatationSpeed) * rotationDegree, 0);
    }

    private void OnEnable()
    {
        meshTransform = rigidb.gameObject.transform;

        Vector3 dir = new Vector3(Random.Range(-1f, 1f), .5f, Random.Range(-1f, 1f));
        rigidb.AddForce(dir * 15);

        Invoke("DisableItem", lifeTime);
    }

    private void DisableItem()
    {
        gameObject.SetActive(false);
    }
}

