﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpawner : MonoBehaviour
{
    public static ItemSpawner Instance;
    public GameObject medkitPrefab;
    public GameObject quiverPrefab;
    public int poolAmount;

    private List<GameObject> medkits = new List<GameObject>();
    private List<GameObject> quivers = new List<GameObject>();

    private void Awake()
    {
        if (Instance != null)
            Destroy(gameObject);

        Instance = this;
    }

    private void Start()
    {
        //pool some medkits and quivers
        for (int i = 0; i < poolAmount; i++)
        {
            GameObject med = Instantiate(medkitPrefab);
            GameObject qui = Instantiate(quiverPrefab);

            med.SetActive(false);
            qui.SetActive(false);

            medkits.Add(med);
            quivers.Add(qui);
        }
    }

    public void CreateMedkitAt(Vector3 position)
    {
        for (int i = 0; i < medkits.Count; i++)
            if (!medkits[i].activeInHierarchy)
            {
                medkits[i].transform.position = position;
                medkits[i].SetActive(true);
                break;
            }
    }

    public void CreateQuiverAt(Vector3 position)
    {
        for (int i = 0; i < quivers.Count; i++)
            if (!quivers[i].activeInHierarchy)
            {
                quivers[i].transform.position = position;
                quivers[i].SetActive(true);
                break;
            }
    }

}
