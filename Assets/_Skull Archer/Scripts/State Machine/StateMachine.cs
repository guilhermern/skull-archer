﻿public class StateMachine<T>
{
    private State<T> currentState;
    private readonly T owner;

    public StateMachine(T owner)
    {
        this.owner = owner;
        currentState = null;
    }

    public void Update()
    {
        currentState?.Update(owner);
    }
    
    public void ChangeState(State<T> newState)
    {
        currentState?.Exit(owner);

        currentState = newState;
        currentState.Enter(owner);
    }
}
