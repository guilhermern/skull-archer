﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;

[CreateAssetMenu(menuName = "Scriptable Objects/Enemy Spawner Settings", fileName = "EnemySpawnerSettings")]
public class EnemySpawnerSettings : ScriptableObject
{   
    [Header("Time range for each spawn in seconds")]
    public float SpawnStartTime;
    public float SpawnFinalTime;

    [Header("Chance in percentage for each type of enemy")]
    [Range(0f, 1f)]
    public float WarriorSpawnChance;
    
    [Range(0f, 1f)]
    public float ArcherSpawnChance;
}
