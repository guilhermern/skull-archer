﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Objects/Object Pool Settings", fileName = "ObjectPoolSettings")]
public class ObjectPoolSettings : ScriptableObject
{
    public List<Pool> Pools;
}
