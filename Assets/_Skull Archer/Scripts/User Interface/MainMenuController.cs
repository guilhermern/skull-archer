﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour
{
    public GameObject loader;
    public GameObject optionsPanel;
    public GameObject buttonsContainer;
    public Button startButton;
    public Button optionsButton;
    public Button exitButton;
    public Button optionsCloseButton;
    public Button optionsSaveButton;
    public Toggle musicToggle;
    public InputField playerArrows;
    public InputField gameTime;

    private void Start()
    {
        startButton.onClick.AddListener(StartGame);
        optionsButton.onClick.AddListener(OpenOptions);
        optionsCloseButton.onClick.AddListener(CloseOptions);
        optionsSaveButton.onClick.AddListener(SaveOptions);
        exitButton.onClick.AddListener(QuitApp);

        // verify if options were saved before
        if (PlayerPrefs.HasKey("config"))
        {
            bool music = PlayerPrefs.GetInt("music") != 0;
            int arrows = PlayerPrefs.GetInt("arrows");
            int time = PlayerPrefs.GetInt("time");

            //print("music " + music);
            //print("Arrows " + arrows);
            //print("time " + time);

            musicToggle.isOn = music;
            playerArrows.text = arrows.ToString();
            gameTime.text = time.ToString();
        }
    }


    private void StartGame()
    {
        buttonsContainer.SetActive(false);
        loader.SetActive(true);

        ApplicationController.Instance.LoadNextScene();
    }

    private void OpenOptions()
    {
        optionsPanel.SetActive(true);
    }

    private void CloseOptions()
    {
        optionsPanel.SetActive(false);
    }

    private void SaveOptions()
    {
        int music = musicToggle.isOn ? 1 : 0;
        int arrows;
        int time;

        bool result = int.TryParse(playerArrows.text, out arrows);

        if (!result) arrows = 60;

        result = int.TryParse(gameTime.text, out time);

        if (!result) time = 180;
        

        //save data at player prefs
        PlayerPrefs.SetInt("music", music);
        PlayerPrefs.SetInt("arrows", arrows);
        PlayerPrefs.SetInt("time", time);
        PlayerPrefs.SetInt("config", 1);
        PlayerPrefs.Save();

        CloseOptions();
    }

    public void QuitApp()
    {
        PlayerPrefs.DeleteAll();
        ApplicationController.Instance.Quit();
    }

}
