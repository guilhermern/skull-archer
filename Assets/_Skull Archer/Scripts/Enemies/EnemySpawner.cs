﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class EnemySpawner : MonoBehaviour
{
    public EnemySpawnerSettings Settings;
    
    [SerializeField] private List<Transform> _spawnPoints;

    private const string WarriorPoolTag = "warrior";
    private const string ArcherPoolTag = "archer";
    
    private float _startTime;
    private float _finalTime;

    private float _sortedTime;
    private float _currentTime;

    private float _warriorSpawnChance;
    private float _archerSpawnChance;

    private bool _isOn = false;
    
    private void Start()
    {
        LoadSettings();
        EnableSpawning();
    }

    private void LoadSettings()
    {
        _startTime = Settings.SpawnStartTime;
        _finalTime = Settings.SpawnFinalTime;

        _warriorSpawnChance = Settings.WarriorSpawnChance;
        _archerSpawnChance = Settings.ArcherSpawnChance;
    }

    private void Update()
    {
        if (!_isOn)
        {
            return;
        }

        UpdateTime();
    }

    private void UpdateTime()
    {
        _currentTime -= Time.deltaTime;

        if (_currentTime > 0f)
        {
            return;
        }

        SpawnEnemy();
        GenerateRandomTime();
    }

    private void SpawnEnemy()
    {
        float randomValue = Random.Range(0f, 1f);
        Vector3 randomPosition = GetRandomSpawnPointPosition();

        if (randomValue <= _warriorSpawnChance)
        {
            Debug.Log("Warrior Spawned");
            ObjectPoolController.Instance.SpawnFromPool(WarriorPoolTag, randomPosition, Quaternion.identity);
        }
        else
        {
            Debug.Log("Archer Spawned");
            ObjectPoolController.Instance.SpawnFromPool(ArcherPoolTag, randomPosition, Quaternion.identity);
        }
    }

    private void GenerateRandomTime()
    {
        _sortedTime = Random.Range(_startTime, _finalTime);
        _currentTime = _sortedTime;
        
        Debug.Log($"Generated time {_sortedTime}");   
    }

    private Vector3 GetRandomSpawnPointPosition()
    {
        int sortedIndex = Random.Range(0, _spawnPoints.Count);

        return _spawnPoints[sortedIndex].position;
    }

    public void EnableSpawning()
    {
        _isOn = true;
        GenerateRandomTime();
    }

    public void DisableSpawning()
    {
        _isOn = false;
        
        _sortedTime = 0f;
        _currentTime = 0f;
    }
}
