﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathState : State<Enemy>
{
    public override void Enter(Enemy owner)
    {
        GameManager.Instance.AddDeadEnemy();
        
        owner.animator.SetBool("Death", true);
        owner.navMeshAgent.isStopped = true;
        
        owner.DesactivateEnemy(3f);
        owner.GetComponent<Collider>().enabled = false;
        owner.TryDropItem();
    }

    public override void Exit(Enemy owner)
    {
        owner.animator.SetBool("Death", false);
    }

    public override void Update(Enemy owner)
    {
        
    }
}
