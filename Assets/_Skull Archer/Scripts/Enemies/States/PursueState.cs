﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PursueState : State<Enemy>
{
    public override void Enter(Enemy owner)
    {
        owner.animator.SetBool("Pursue", true);
        owner.navMeshAgent.SetDestination(GameManager.Instance.player.transform.position);
    }

    public override void Exit(Enemy owner)
    {
        Debug.Log("Exit pursue");
        owner.animator.SetBool("Pursue", false);
    }

    public override void Update(Enemy owner)
    {
        Debug.Log("Update pursue");

        owner.navMeshAgent.SetDestination(GameManager.Instance.player.transform.position);

        if (owner.navMeshAgent.remainingDistance > owner.distanceToAttack)
        {
            owner.navMeshAgent.SetDestination(GameManager.Instance.player.transform.position);
        }

        if (owner.navMeshAgent.remainingDistance < owner.distanceToAttack)
        {
            owner.stateMachine.ChangeState(new AttackState());
        }
    }
}
