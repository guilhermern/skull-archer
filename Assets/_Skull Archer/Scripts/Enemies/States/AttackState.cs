﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackState : State<Enemy>
{
    public override void Enter(Enemy owner)
    {
        owner.animator.SetBool("Attack", true);
    }

    public override void Exit(Enemy owner)
    {
        owner.animator.SetBool("Attack", false);
    }

    public override void Update(Enemy owner)
    {
        owner.LookAtPlayer();
        
        if (owner.distanceToPlayer > owner.distanceToAttack)
        {
            owner.stateMachine.ChangeState(new PursueState());
        }
    }
}
