﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPoolController : MonoBehaviour
{
    public static ObjectPoolController Instance;
    
    public ObjectPoolSettings Settings;
    
    
    private Dictionary<string, Queue<GameObject>> _pool;
    private List<Pool> _poolsList;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        _pool = new Dictionary<string, Queue<GameObject>>();
        
        LoadSettings();
        CreatePools();
    }

    private void LoadSettings()
    {
        _poolsList = Settings.Pools;
    }
    
    private void CreatePools()
    {
        foreach (Pool pool in _poolsList)
        {
            Queue<GameObject> objectPool = new Queue<GameObject>();

            for (int i = 0; i < pool.size; i++)
            {
                GameObject obj = Instantiate(pool.prefab, transform);
                obj.SetActive(false);
                
                objectPool.Enqueue(obj);
            }
            
            _pool.Add(pool.tag, objectPool);
        }
    }

    public GameObject SpawnFromPool(string poolTag, Vector3 position, Quaternion rotation)
    {
        if (!_pool.ContainsKey(poolTag))
        {
            Debug.LogWarning("Pool tag does not exist");
            return null;
        }
        
        GameObject obj = _pool[poolTag].Dequeue();
        
        obj.SetActive(true);
        obj.transform.SetParent(null);
        obj.transform.position = position;
        obj.transform.rotation = rotation;

        IObjectPooled objPooled = obj.GetComponent<IObjectPooled>();
        
        objPooled?.OnObjectPooled();

        return obj;
    }

    public void DeactivateObject(string poolTag, GameObject obj)
    {
        if (!_pool.ContainsKey(poolTag))
        {
            Debug.LogWarning("Pool tag does not exist");
            return;
        }

        obj.SetActive(false);
        obj.transform.position = Vector3.zero;
        obj.transform.rotation = Quaternion.identity;
        obj.transform.SetParent(transform);
        
        _pool[poolTag].Enqueue(obj);
    }



}
