﻿public interface IObjectPooled
{
    void OnObjectPooled();
}
